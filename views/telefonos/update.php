<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Telefonos */

$this->title = 'Update Telefonos: ' . $model->codigo_establecimiento;
$this->params['breadcrumbs'][] = ['label' => 'Telefonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_establecimiento, 'url' => ['view', 'codigo_establecimiento' => $model->codigo_establecimiento, 'telefonos' => $model->telefonos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="telefonos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
