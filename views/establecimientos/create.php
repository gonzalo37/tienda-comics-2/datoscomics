<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Establecimientos */

$this->title = 'Create Establecimientos';
$this->params['breadcrumbs'][] = ['label' => 'Establecimientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="establecimientos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
