<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Comics */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comics-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_numerico')->textInput() ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'coleccion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'n_dibujante')->textInput() ?>

    <?= $form->field($model, 'codigo_dibujante')->textInput() ?>

    <?= $form->field($model, 'codigo_editorial')->textInput() ?>

    <?= $form->field($model, 'portada')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripción')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'leído')->textInput() ?>

    <?= $form->field($model, 'favorito')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
