<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Comics';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comics-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Comics', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_numerico',
            'nombre',
            'coleccion',
            'n_dibujante',
            'codigo_dibujante',
            //'codigo_editorial',
            //'portada',
            //'descripción',
            //'leído',
            //'favorito',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
