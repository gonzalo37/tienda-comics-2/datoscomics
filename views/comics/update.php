<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Comics */

$this->title = 'Update Comics: ' . $model->codigo_numerico;
$this->params['breadcrumbs'][] = ['label' => 'Comics', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_numerico, 'url' => ['view', 'codigo_numerico' => $model->codigo_numerico, 'n_dibujante' => $model->n_dibujante]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="comics-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
