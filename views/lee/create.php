<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lee */

$this->title = 'Create Lee';
$this->params['breadcrumbs'][] = ['label' => 'Lees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lee-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
