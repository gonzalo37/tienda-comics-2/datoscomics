<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $codigo
 * @property string|null $dni
 * @property string|null $nombre
 * @property string|null $teléfono
 *
 * @property Lee[] $lees
 * @property Comics[] $codigoNumericoComics
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'integer'],
            [['dni'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 20],
            [['teléfono'], 'string', 'max' => 12],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'teléfono' => 'Teléfono',
        ];
    }

    /**
     * Gets query for [[Lees]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLees()
    {
        return $this->hasMany(Lee::className(), ['codigo_usuario' => 'codigo']);
    }

    /**
     * Gets query for [[CodigoNumericoComics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoNumericoComics()
    {
        return $this->hasMany(Comics::className(), ['codigo_numerico' => 'codigo_numerico_comic'])->viaTable('lee', ['codigo_usuario' => 'codigo']);
    }
}
