<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dibujantes".
 *
 * @property int $codigo
 * @property string|null $nombre
 * @property string|null $apellido1
 * @property string|null $apellido2
 *
 * @property Comics[] $comics
 */
class Dibujantes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dibujantes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'integer'],
            [['nombre', 'apellido1', 'apellido2'], 'string', 'max' => 20],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'apellido1' => 'Apellido1',
            'apellido2' => 'Apellido2',
        ];
    }

    /**
     * Gets query for [[Comics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComics()
    {
        return $this->hasMany(Comics::className(), ['codigo_dibujante' => 'codigo']);
    }
}
