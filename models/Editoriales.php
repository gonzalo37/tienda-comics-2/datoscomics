<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "editoriales".
 *
 * @property int $codigo
 * @property string|null $nombre
 *
 * @property Comics[] $comics
 */
class Editoriales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'editoriales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Comics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComics()
    {
        return $this->hasMany(Comics::className(), ['codigo_editorial' => 'codigo']);
    }
}
