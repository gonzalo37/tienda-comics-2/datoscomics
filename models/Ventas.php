<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ventas".
 *
 * @property int $id
 * @property int|null $codigo_establecimiento
 * @property int|null $codigo_numerico_comic
 *
 * @property Comics $codigoNumericoComic
 * @property Establecimientos $codigoEstablecimiento
 */
class Ventas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'codigo_establecimiento', 'codigo_numerico_comic'], 'integer'],
            [['id'], 'unique'],
            [['codigo_numerico_comic'], 'exist', 'skipOnError' => true, 'targetClass' => Comics::className(), 'targetAttribute' => ['codigo_numerico_comic' => 'codigo_numerico']],
            [['codigo_establecimiento'], 'exist', 'skipOnError' => true, 'targetClass' => Establecimientos::className(), 'targetAttribute' => ['codigo_establecimiento' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_establecimiento' => 'Codigo Establecimiento',
            'codigo_numerico_comic' => 'Codigo Numerico Comic',
        ];
    }

    /**
     * Gets query for [[CodigoNumericoComic]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoNumericoComic()
    {
        return $this->hasOne(Comics::className(), ['codigo_numerico' => 'codigo_numerico_comic']);
    }

    /**
     * Gets query for [[CodigoEstablecimiento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEstablecimiento()
    {
        return $this->hasOne(Establecimientos::className(), ['codigo' => 'codigo_establecimiento']);
    }
}
