<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lee".
 *
 * @property int $id
 * @property int|null $codigo_usuario
 * @property int|null $codigo_numerico_comic
 *
 * @property Comics $codigoNumericoComic
 * @property Usuarios $codigoUsuario
 */
class Lee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'codigo_usuario', 'codigo_numerico_comic'], 'integer'],
            [['codigo_usuario', 'codigo_numerico_comic'], 'unique', 'targetAttribute' => ['codigo_usuario', 'codigo_numerico_comic']],
            [['id'], 'unique'],
            [['codigo_numerico_comic'], 'exist', 'skipOnError' => true, 'targetClass' => Comics::className(), 'targetAttribute' => ['codigo_numerico_comic' => 'codigo_numerico']],
            [['codigo_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['codigo_usuario' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_usuario' => 'Codigo Usuario',
            'codigo_numerico_comic' => 'Codigo Numerico Comic',
        ];
    }

    /**
     * Gets query for [[CodigoNumericoComic]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoNumericoComic()
    {
        return $this->hasOne(Comics::className(), ['codigo_numerico' => 'codigo_numerico_comic']);
    }

    /**
     * Gets query for [[CodigoUsuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['codigo' => 'codigo_usuario']);
    }
}
