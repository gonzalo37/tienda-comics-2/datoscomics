<?php

namespace app\controllers;

use Yii;
use app\models\Comics;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ComicsController implements the CRUD actions for Comics model.
 */
class ComicsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Comics models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Comics::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Comics model.
     * @param integer $codigo_numerico
     * @param integer $n_dibujante
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_numerico, $n_dibujante)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_numerico, $n_dibujante),
        ]);
    }

    /**
     * Creates a new Comics model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Comics();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_numerico' => $model->codigo_numerico, 'n_dibujante' => $model->n_dibujante]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Comics model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $codigo_numerico
     * @param integer $n_dibujante
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_numerico, $n_dibujante)
    {
        $model = $this->findModel($codigo_numerico, $n_dibujante);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_numerico' => $model->codigo_numerico, 'n_dibujante' => $model->n_dibujante]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Comics model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $codigo_numerico
     * @param integer $n_dibujante
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_numerico, $n_dibujante)
    {
        $this->findModel($codigo_numerico, $n_dibujante)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Comics model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $codigo_numerico
     * @param integer $n_dibujante
     * @return Comics the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_numerico, $n_dibujante)
    {
        if (($model = Comics::findOne(['codigo_numerico' => $codigo_numerico, 'n_dibujante' => $n_dibujante])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
